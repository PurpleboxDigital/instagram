<?php
global $wpdb, $instagram_scroller_options;
ksort($instagram_scroller_options);
?>

<div class="wrap">
    <div class="instagram_scroller_settings">
        <h1 class="wp-heading-inline">Instagram Scroller</h1>

        <?php if (!empty($_GET['reset-plugin']) && $_GET['reset-plugin'] == "instagram-scroller") { ?>
            <div id="setting-error-settings_updated" class="updated settings-error notice">
                <p>
                    <strong>Success: Plugin has been been reset. All <b>Instagram Scroller</b> data and images have been deleted.</strong>
                    <a href="options-general.php?page=instagram-scroller-settings.php">[Refresh Page]</a>
                </p>
            </div>
        <?php } ?>

        <h2>Introduction</h2>
        <p>This plugin will automatically extract images from your Instagram feed and allow you to display them on your website.
            All images are saved locally ('plugins/instagram-scroller/images/') and image details are saved in a database table ('<?php echo $wpdb->prefix;?>_instagram_scroller').
            Additionally, all images are lazy loaded so they won't be requested until they are visible on screen.
            The plugin will make 1 Instagram API request every <i>refresh rate</i> value set below.
        </p>
        <h2>Shortcode</h2>
        <p>To add the Instagram Scroller to your posts/pages,</p>
        <div class="shortcode">
            [instagram-scroller max-images=<b>XX</b>]
        </div>
        <p>To add the Instagram Scroller to your template,</p>
        <div class="shortcode">
            &lt;?php echo do_shortcode("[instagram-scroller max-images=<b>XX</b>]"); ?&gt;
        </div>
        <p><i>Note: Replace XX with the total images you want to display (eg. max-images=10)</i></p>
        <h2>Settings</h2>
        <p>You will need an Instagram Access Token to display the Instagram feed.</p>
        <p>To enable the plugin to regularly check for updated Instagram posts, set the Refresh Rate value (in seconds, 3600 = 1 hour).</p>
        <form method="post" action="options.php">
            <?php settings_fields( 'instagram_scroller_options_group' ); ?>
            <table>
                <tr valign="top">
                    <th>
                        Name
                    </th>
                    <th>
                        Value
                    </th>
                </tr>
                <tr valign="top">
                    <th>
                        <hr>
                    </th>
                    <th>
                        <hr>
                    </th>
                </tr>

                <?php foreach($instagram_scroller_options as $key => $value) {
                    $label = str_replace("instagram_scroller_", "", $key);
                    $label = str_replace("_", " ", $label);
                    $label = ucwords($label);
                    ?>
                    <tr valign="top">
                        <th scope="row">
                            <label for="<?php echo $key;?>"><?php echo $label;?></label>
                        </th>
                        <td>
                            <?php if($key != "instagram_scroller_use_default_template") {?>
                                <input class="field" type="text" id="<?php echo $key;?>" name="<?php echo $key;?>" value="<?php echo get_option($key); ?>" />
                            <?php } else { ?>
                                <select class="field" id="<?php echo $key;?>" name="<?php echo $key;?>">
                                    <option value="yes" <?php if(get_option($key) == "yes") { echo " selected='selected' "; }?>>Yes</option>
                                    <option value="no" <?php if(get_option($key) == "no") { echo " selected='selected' "; }?>>No</option>
                                </select> If no, see <strong>Custom Template</strong> option below
                            <?php } ?>
                        </td>
                    </tr>
                <?php }?>
                    <tr valign="top">
                        <th scope="row">
                            <label for="Reset Plugin">

                            </label>
                        </th>
                        <td style="text-align:left;">
                            <p>
                                <a class="btn btn-danger" href="options-general.php?page=instagram-scroller-settings.php&reset-plugin=instagram-scroller">Reset Plugin</a>
                            </p>
                        </td>
                    </tr>
            </table>
            <p>
                <i>Note: Resetting the plugin will delete all Instagram Scroller data and images. The next time the plugin is loaded on the front-end of the website the data and images will be refreshed.</i>
            </p>
            <?php  submit_button(); ?>
        </form>

        <h2>Custom Template</h2>
        <p>To create a custom template for your theme follow the instructions below,</p>
        <div class="shortcode">
            <?php
            $file = file_get_contents(plugin_dir_path(__FILE__).'template.example.txt');
            echo "<pre>";
            print_r(str_replace("<", "&lt;", $file));
            echo "</pre>";
            ?>
        </div>

    </div>
</div>

<style>
.instagram_scroller_settings h2 {
    margin-top: 20px;
}
.instagram_scroller_settings table {
    width: 100%;
}
.instagram_scroller_settings th,
.instagram_scroller_settings tr {
     text-align: left;
     width: 50%;
}
.instagram_scroller_settings tr {
    text-align: left;
    width: 50%;
}
.instagram_scroller_settings input[type=text] {
    width: 100%;
}
.instagram_scroller_settings .shortcode {
    border: 1px solid #0073aa;
    border-radius: 7px;
    background-color: #0073aa;
    padding: 10px;
    color: #ffffff;
}
.instagram_scroller_settings .reset {
    border: 1px solid #c3e6cb;
    border-radius: 7px;
    background-color: #c3e6cb;
    padding: 10px;
    color: #155724;
}
.instagram_scroller_settings .btn-danger {
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545;
    padding: 5px;
    border-radius: 3px;
    cursor: pointer;
}
.instagram_scroller_settings a {
    text-decoration: none;
}
</style>