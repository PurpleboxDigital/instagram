<?php
if (!empty($images)): ?>
    <!-- Slideshow outer container -->
    <div class="col-12 col-md-12 p-0 slideshow d-flex">

        <!-- Slideshow inner container -->
        <div class="slideshow-container p-5 d-flex flex-row">

            <?php
            {
                $count = 1;
                foreach ($images as $image) {
                    ?>

                    <!-- Slide container //-->
                    <div class="slides" id="slide-<?php echo $count; ?>">
                        <a href="<?php echo $image->link; ?>" target="_blank">
                            <img src="<?php echo plugins_url() . '/instagram-scroller/images/loader.jpg'; ?>"
                                 data-src="<?php echo $image->images->thumbnail->url; ?>"
                                 height="<?php echo $image->images->thumbnail->height; ?>"
                                 width="<?php echo $image->images->thumbnail->width; ?>"
                                 alt="<?php echo stripslashes($image->caption->text); ?>"
                                 class="lazyload">
                        </a>
                    </div>

                    <?php
                    $count++;
                }
            }
            ?>

        </div>

        <!-- Next and previous buttons -->
        <div class="mt-auto mb-2 pr-1">
            <a class="prev" onClick="plusSlides(-1)">&#10094;</a>
            <a class="next" onClick="plusSlides(1)">&#10095;</a>
        </div>

    </div>
<?php endif; ?>