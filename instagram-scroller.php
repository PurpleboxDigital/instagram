<?php
/**
 * Plugin Name: Instagram Scroller
 * Description: Displays Instagram images using the Instagram API feed.
 * Version: 1.0.0
 * Author: JeffL
 */

/**
 * Reset Plugin Request
 */
if (!empty($_GET['reset-plugin']) && $_GET['reset-plugin'] == "instagram-scroller")
{
    instagram_scroller_reset_plugin ();
}

/**
 * Init Plugin
 */
function instagram_scroller ( $atts = '' )
{
    global $wpdb;

    // if we are in the admin area we dont need to load the front end plugin
    if (is_admin())
    {
        return;
    }

    // get/set default values for Instagram Scroller
    $access_token = (!empty(get_option('instagram_scroller_access_token'))) ? get_option('instagram_scroller_access_token') : '';
    $refresh_rate_seconds = (!empty(get_option('instagram_scroller_refresh_rate_seconds'))) ? get_option('instagram_scroller_refresh_rate_seconds') : '3600';
    $use_default_template = (!empty(get_option('instagram_scroller_use_default_template'))) ? get_option('instagram_scroller_use_default_template') : 'yes';

    // set default values for shortcode attributes
    $values = shortcode_atts(array (
        'max-images' => 5
    ), $atts);

    // set directory paths to store/display images
    $uploads_folder = wp_get_upload_dir();
    $set_upload_dir = $uploads_folder['basedir']."/instagram";
    $get_upload_dir = $uploads_folder['baseurl']."/instagram";

    // get data from database table
    $resultdb = $wpdb->get_results("SELECT * 
                                           FROM " . $wpdb->prefix . "_instagram_scroller 
                                           ORDER BY id 
                                           ASC LIMIT " . (int)$values['max-images']);
    // array for storing results from db
    $res = array ();

    // if we have results from the db
    if (!empty($resultdb))
    {
        // we need to re-format the data from the database as it's not in the same structure as the Instagram API
        foreach($resultdb as $result)
        {
            $r = new stdClass();
            @$r->user->id = $result->user_id;
            $r->user->username = $result->user_name;
            $r->images = new stdClass();
            $r->images->thumbnail = new stdClass();
            $r->images->thumbnail->width = $result->thumbnail_height;
            $r->images->thumbnail->height = $result->thumbnail_height;
            $r->images->thumbnail->url = $get_upload_dir.'/'.$result->thumbnail_url;
            $r->images->low_resolution = new stdClass();
            $r->images->low_resolution->width = $result->low_resolution_width;
            $r->images->low_resolution->height = $result->low_resolution_height;
            $r->images->low_resolution->url = $get_upload_dir.'/'.$result->low_resolution_url;
            $r->images->standard_resolution = new stdClass();
            $r->images->standard_resolution->width = $result->standard_resolution_width;
            $r->images->standard_resolution->height = $result->standard_resolution_height;
            $r->images->standard_resolution->url = $get_upload_dir.'/'.$result->standard_resolution_url;
            $r->caption = new stdClass();
            $r->caption->text = $result->description;
            $r->link = $result->link_url;
            $r->type = $result->type;

            $res[] = $r;
        }

        // WAIT! - lets get the time of the last update
        $last_entry = date("U", strtotime($resultdb[0]->date_added));

        // if the results in our database table are older than our refresh rate,
        if ( (date("U") - $last_entry) > $refresh_rate_seconds )
        {
            // set db result to false
            // ( so we can get data from the Instagram API instead )
            $resultdb = false;
        }

    }

    // if we dont have any database results or we need fresh results, we need to get the data from the Instagram API
    if(empty($resultdb)) {
        // Instagram API
        $resultsInstagram = @file_get_contents('https://api.instagram.com/v1/users/self/media/recent/?access_token=' . trim($access_token) . '&count=' . (int)$values['max-images']);
        $resultsInstagram = json_decode($resultsInstagram);

        // truncate database table
        // ( we dont need to store more instagram api data than we can actually use )
        $wpdb->query('TRUNCATE ' . $wpdb->prefix . '_instagram_scroller');

        // for each image we have from the Instagram API
        if (!empty($resultsInstagram->data))
        {
            foreach ($resultsInstagram->data as $image)
            {
                // if images have not already been downloaded from Instagram, download images to our local server
                if (!file_exists($set_upload_dir . '/images/thumbnail/' . $image->id . '_t.jpg')) {
                    copy($image->images->thumbnail->url, $set_upload_dir . '/images/thumbnail/' . $image->id . '_t.jpg');
                }
                if (!file_exists($set_upload_dir . '/images/low_resolution/' . $image->id . '_l.jpg')) {
                    copy($image->images->low_resolution->url, $set_upload_dir . '/images/low_resolution/' . $image->id . '_l.jpg');
                }
                if (!file_exists($set_upload_dir . '/images/standard_resolution/' . $image->id . '_s.jpg')) {
                    copy($image->images->standard_resolution->url, $set_upload_dir . '/images/standard_resolution/' . $image->id . '_s.jpg');
                }

                // sanitise the instagram api description field ( it could be full of anything! )
                $str = filter_var($image->caption->text, FILTER_UNSAFE_RAW, FILTER_FLAG_ENCODE_LOW | FILTER_FLAG_STRIP_HIGH);
                $str = addslashes($str);

                // insert the data into our database table
                $wpdb->insert($wpdb->prefix . "_instagram_scroller", array(
                    'user_id' => $image->user->id,
                    'user_name' => $image->user->username,
                    'thumbnail_url' => 'images/thumbnail/' . $image->id . '_t.jpg',
                    'thumbnail_height' => $image->images->thumbnail->height,
                    'thumbnail_width' => $image->images->thumbnail->width,
                    'low_resolution_url' => 'images/low_resolution/' . $image->id . '_l.jpg',
                    'low_resolution_height' => $image->images->low_resolution->height,
                    'low_resolution_width' => $image->images->low_resolution->width,
                    'standard_resolution_url' => 'images/standard_resolution/' . $image->id . '_s.jpg',
                    'standard_resolution_height' => $image->images->standard_resolution->height,
                    'standard_resolution_width' => $image->images->standard_resolution->width,
                    'description' => $str,
                    'link_url' => $image->link,
                    'type' => $image->type,
                    'date_added' => date("Y-m-d H:i:s")
                ));
            }
        }

        $apiData = (!empty($resultsInstagram->data)) ? $resultsInstagram->data : array();
    }

    // set which data to use ( our database data OR Instagram API feed )
    $images = (!empty($res)) ? $res : $apiData;

    // if we dont have any image data from the Instagram API or our database
    if(empty($images))
    {
        echo "Instagram data currently not available";
    }
    // else, choose which template we should show
    else
    {
        if ($use_default_template == "yes")
        {
            // load plugin default template
            include("template.php");
        }
        else
        {
            // OR, return object for custom template
            // ( copy contents of /template.php to use as base template )
            return json_encode($images);
        }
    }
}
add_shortcode('instagram-scroller', 'instagram_scroller');

/**
 * Options
 */
global $instagram_scroller_options;

$instagram_scroller_options = array (
    "instagram_scroller_access_token" => "",
    "instagram_scroller_refresh_rate_seconds" => "3600",
    "instagram_scroller_use_default_template" => "yes",
);

/**
 * Options Page
 */
function instagram_scroller_admin_menu_item ()
{
    add_options_page (
        'Instagram Scroller',
        'Instagram Scroller',
        'manage_options',
        'instagram-scroller-settings.php',
        'instagram_scroller_options_page'
    );
}
add_action( 'admin_menu', 'instagram_scroller_admin_menu_item' );

/**
 * Options Page Content
 */
function instagram_scroller_options_page ()
{
    include("settings.php");
}

/**
 * Option Register Settings (Form Fields)
 */
function instagram_scroller_register_settings ()
{
    global $instagram_scroller_options;

    foreach ($instagram_scroller_options as $key => $value)
    {
        // register each form field and it's default value
        add_option($key, $value);
        register_setting('instagram_scroller_options_group', $key, 'instagram_scroller_callback');
    }
}
add_action( 'admin_init', 'instagram_scroller_register_settings' );

/**
 * On Plugin Activation, Create MySQL Tables and Media Folders
 */
function instagram_scroller_plugin_activated ()
{
    instagram_scroller_create_data_table ();
    instagram_scroller_create_media_folders ();
}
register_activation_hook( __FILE__, 'instagram_scroller_plugin_activated' );

/**
 * Create MySQL Table
 */
function instagram_scroller_create_data_table ()
{
    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . "_instagram_scroller (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `user_id` varchar(50) DEFAULT NULL,
                `user_name` varchar(50) DEFAULT NULL,
                `thumbnail_url` text,
                `thumbnail_height` int(11) DEFAULT NULL,
                `thumbnail_width` int(11) DEFAULT NULL,
                `low_resolution_url` text,
                `low_resolution_height` int(11) DEFAULT NULL,
                `low_resolution_width` int(11) DEFAULT NULL,
                `standard_resolution_url` text,
                `standard_resolution_height` int(5) DEFAULT NULL,
                `standard_resolution_width` int(5) DEFAULT NULL,
                `description` text,
                `link_url` text,
                `type` varchar(50) DEFAULT NULL,
                `date_added` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
                ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

/**
 * Create Folders to Store Media
 */
function instagram_scroller_create_media_folders ()
{
    // set directory path to image folder(s)
    $uploads_folder = wp_get_upload_dir();
    $set_upload_dir = $uploads_folder['basedir']."/instagram";

    // set folder(s) for each image size
    $folders = array(
        'thumbnail'             => 'thumbnail',
        'low_resolution'        => 'low_resolution',
        'standard_resolution'   => 'standard_resolution'
    );
    foreach ($folders as $key => $value)
    {
        if (!file_exists($set_upload_dir . '/images/' . $value))
        {
            // create folder for storing image(s)
            mkdir($set_upload_dir . '/images/' . $value, 0755, true);
        }
    }
}

/**
 * Reset Plugin
 */
function instagram_scroller_reset_plugin ()
{
    global $wpdb;

    // set directory paths to store/display images
    $uploads_folder = wp_get_upload_dir();
    $set_upload_dir = $uploads_folder['basedir']."/instagram";

    // delete all .jpg images in images/thumbnail/ folder
    foreach (glob($set_upload_dir . "/images/thumbnail/*.jpg") as $file)
    {
        unlink($file);
    }
    // delete all .jpg images in images/standard_resolution/ folder
    foreach (glob($set_upload_dir . "/images/standard_resolution/*.jpg") as $file)
    {
        unlink($file);
    }
    // delete all .jpg images in images/low_resolution/ folder
    foreach (glob($set_upload_dir . "/images/low_resolution/*.jpg") as $file)
    {
        unlink($file);
    }
    // reset database table
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . '_instagram_scroller');
}

/**
 * Add Settings Link To Plugin Page
 */
function instagram_scroller_plugin_page_settings_link( $links )
{
    $links[] = '<a href="' . admin_url( 'options-general.php?page=instagram-scroller-settings.php' ) . '">' . __('Settings') . '</a>';
    return $links;
}
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'instagram_scroller_plugin_page_settings_link');

/**
 * Load CSS
 */
function instagram_scroller_load_plugin_css()
{
    wp_enqueue_style('instagram-scroller-css', plugin_dir_url(__FILE__). 'css/styles.css');
}

/**
 * Load JS
 */
function instagram_scroller_load_plugin_js()
{
    wp_enqueue_script( 'instagram-scroller-js', plugin_dir_url(__FILE__). 'js/script.js');
    wp_enqueue_script( 'lazy-load-js', plugin_dir_url(__FILE__). 'js/lazy-load.js');
}

/**
 * Load Enqueued Scripts in the Footer
 */
function instagram_scroller_footer_enqueue_scripts()
{
    add_action('wp_footer', 'instagram_scroller_load_plugin_css', 5 );
    add_action('wp_footer', 'instagram_scroller_load_plugin_js', 5 );
}
add_action('after_setup_theme', 'instagram_scroller_footer_enqueue_scripts');