var slideIndex = 1;
var total_images = 5;
showSlides(slideIndex);
function plusSlides(n)
{
    showSlides(slideIndex += n);
}
function showSlides(n)
{
    var i;
    var s;
    var slides = document.getElementsByClassName("slides");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) { slideIndex = 1 }
    if (n > ( ( slides.length - total_images) + 1 ) ) { slideIndex =  ( slides.length - total_images) + 1 ; }

    for (i = 0; i < slides.length ; i++)
    {
        if(slides[i])
        {
            slides[i].style.display = "none";
        }
    }

    for(s = (2 - total_images); s <= 1; s++)
    {
        if(slides[slideIndex - s])
        {
            slides[slideIndex - s].style.display = "block";
        }
    }
}